<?php

/**
 * Implements hook_variable_info().
 */
function deeptabs_deep_variable_info() {
  $variables['deeptabs_deep_patterns'] = array(
    'type' => 'text',
    'title' => t('Path patterns for Deep Tabs Deep'),
    'description' => t('Patterns, each on one line. Use * for multi-level wildcard.'),
    'default' => '*',
  );
  return $variables;
}
